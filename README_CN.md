![](https://img-blog.csdnimg.cn/img_convert/6add5a7520e4744c2d879c9c955d40db.png)

## ERD Online可以帮你做...
📦 开箱即用，将注意力集中在数据结构设计上

📋 快速复制已有表结构、json 生成表

🏷 在线管理表结构，支持正向向数据库执行

🎨 将已有的数据库结构解析到软件中管理

📱 支持多种数据库连接在线管理（Mysql、Oracle、DB2、SqlServer、PostGreSql）

📡 每个需求与变动，都可以生成版本，每个版本之间可以比对差异

🎉 可将所有表结构，自动生成 word、html、md 文档，便于线下流动
。。。

## 甚至可以帮你定义业务领域的数据词典....
## 核心功能
- 元数据解析
- 元数据管理
- 多数源支持（mysql、oracle、sqlserver、postgresql）
- 多数源同步
- 在线导出sql（全量或差量）
- 在线执行sql
- 数据血缘管理
- 团队协作
- 元数据导出（word、pdf、html、markdown）
- 官方市场一键导入
- 低代码平台（待建设）
	- BI
	- 大屏
	- 低代码平台


## 关于我们

新版本源码 Github: [https://github.com/www-zerocode-net-cn/ERD-Online](hhttps://github.com/www-zerocode-net-cn/ERD-Online)

ERD Online 详细文档:  [https://portal.zerocode.net.cn](https://portal.zerocode.net.cn)

## 4.0.7发布，增加新春火红主题
![https://github.com/www-zerocode-net-cn/ERD-Online/discussions/24](https://github.com/www-zerocode-net-cn/ERD-Online/discussions/24)

## 4.0.5全新来袭🚀🚀🚀🚀🚀🚀🚀🚀🚀

>- 全新升级，团队功能、权限管理、更美更稳定
>- 从这个版本，我们隆重推出低代码设计平台LOCO，见下文

## 发展里程碑

![](https://img-blog.csdnimg.cn/img_convert/a3802133685ab857f8101ef9bc0aa6df.png)

## 4.0.5改动一览

### 功能完善
### 数据查询功能

![数据查询](https://img-blog.csdnimg.cn/img_convert/080c3e19296c0232d561e09bea4f41d1.png)

数据查询：
- 快速查询数据库中数据，自动分页，有了这功能，基本就可以放弃navicate等收费软件了
- 支持树结构多级保存查询，方便自定义查询结构
- 在线保存全部查询记录，查询记录留档（公共财产），避免某些业务只有个别人了解，人走茶凉

![执行计划](https://img-blog.csdnimg.cn/img_convert/cc45c157c7af7b8fb779ec3b837b205c.png)

执行计划：有助于分析sql的执行效率，快速定位解决生产慢sql

![历史查询记录](https://img-blog.csdnimg.cn/img_convert/9acc90353eb5f0a15638f967769ae7ef.png)

历史查询记录：可以追溯历史全部使用人执行的sql


### 调整模板框底色为白色

![调整底色](https://img-blog.csdnimg.cn/img_convert/1bb019872a22643a6f4e6efd2d22720a.png)

调整原来的深色背景为白色，整体风格更加协调

### 调整默认值设置方式

![默认值设置方式](https://img-blog.csdnimg.cn/img_convert/372d8725d1dc600180a4d394206cfea0.png)

调整默认值设置方式：
- 字符型的默认值用单引号包起来
- 整数直接写number型数据
- 同时支持设置时间类型的默认值：CURRENT_TIMESTAMP
- 基本满足了开发周期中大部分设置场景

### 解决ERD、PDman导入数据异常问题

![导入异常](https://img-blog.csdnimg.cn/img_convert/e1d6cce62e66698a7048a6f842838674.png)

ERD、PDman导入数据后，数字类型丢失问题解决！

手动添加的表没有这个问题！

#### 个人项目
个人项目即原有的项目管理，每个账号只能编辑自己的「个人项目」。
![](https://img-blog.csdnimg.cn/img_convert/c101e606a51827ba73d3cb97ab2336a9.png)

#### 团队项目
新增团队项目，支持多人多账户共同维护一份元数据模型。管理员可以通过邮件、以及账号邀请新用户加入。
![](https://img-blog.csdnimg.cn/img_convert/be6060a973bb71ae1d768caef2ced081.png)


#### 权限控制
针对团队项目，提供细致的权限控制能力，对页面、元素、按钮进行权限划分，使每个角色具备最小权限单元，避免生产事故。

![](https://img-blog.csdnimg.cn/img_convert/09e04381fdbe2b22a60ead1f33aa5fa3.png)

### 风格转变
#### 管理风格
页面整体风格由之前的设计风格转变到管理风格，更加贴合企业级使用需求。

![](https://img-blog.csdnimg.cn/img_convert/b0674f53bfdf4d3ba42ffe8fdf8c3ae9.png)

### 生成文档
![在这里插入图片描述](https://img-blog.csdnimg.cn/30bc02e413b149eb98f08651f8ff7931.png)

![在这里插入图片描述](https://img-blog.csdnimg.cn/c98cb52301da4bc8b11885d5b3214799.png)


### 升级
#### 4.0.0~4.0.4
低版本的元数据模型，使用「导出ERD」功能，将模型导出，然后在4.0.5上使用「导入ERD」功能，即可将模型全部导入。
#### 4.0.5
[一键安装](https://portal.zerocode.net.cn/docs/quick-start/try-out-install)

### 产品矩阵
#### ERD Online
ERD Online 为开发者提供一站式的数据库结构设计、版本管理、执行 sql、逆向解析、文档生成功能。🎉**

ERD Online 的目标：通过对元数据管理，向大数据、低代码、BI、大屏应用等赋能，让数据成为企业创新的核心引擎。

![](https://img-blog.csdnimg.cn/img_convert/32f0cf3492f90264fbcb2fdea16d8fe1.png)


#### 低代码平台（LOCO）
低代码是未来软件生态的重要一环，也是必不可少的一环，从这个版本开始，我们除了会持续在元数据建模上发力，也会加大在低代码系统的投入，为我们最终的愿景添砖加瓦。

LOCO 的目标：借助ERD Online 元数据管理能力，打通低代码元数据管理环节，在应用侧成为企业数字化转型的得力助手。

体验地址：[https://loco.zerocode.net.cn/](https://loco.zerocode.net.cn/)

![](https://img-blog.csdnimg.cn/img_convert/77a42d62b633e8872c40e4bcc8affa85.png)
## 4.0.5使用指南
4.0.5使用指南：[https://portal.zerocode.net.cn/docs/quick-start/a-basic-project](https://portal.zerocode.net.cn/docs/quick-start/a-basic-project)




## 案例说明
> 某公司涉及多套关系数据库，拥有1000多个表，涉及将近10000多个字段，一套良好的建模类型血缘关系展示，可以快速的让业务人员，营销，运维，产品，后端，前端，大数据，BI，数据分析人员 快速熟悉数据库架构以及字段类型意图。根据前端展示快速理解业务意图，变相极大的节省公司成本

- 某公司BI人员入职，作为一个sqlboy，针对以上10000多字段，没有管理系统的前提下，需要去梳理数据库字段文档，理解字段意图，理解公司业务架构，最后输出BI报表，至少存在1个月的沉默成本。
- 公司新入职产品经理，涉及多个系统架构需求，需要理解老系统无数表字段情况下，才能对系统做新功能需求更新
- 一套数据库有上千个表的情况下，要让公司研发能高效调动调配处理开发，一个程序员理解系统表跟字段需要花费非常长的时间，系统可以节省研发数倍的成本。
- 公司内部多套数据库进行字段以及表新增，需要领导进行审计放行，该平台可以统一化管理数据库，并进行审计放行，且存在历史版本回溯比对。


##  巨人的肩膀

- React[(https://reactjs.org](https://reactjs.org))
- font-awesome([http://www.fontawesome.com.cn](http://www.fontawesome.com.cn))
- AntV-G6 ([http://antvis.github.io/g6/doc/index.html](http://antvis.github.io/g6/doc/index.html)) 
- highlightjs([https://highlightjs.org](https://highlightjs.org))
- ace editor([https://ace.c9.io](https://ace.c9.io))
- doT.js([http://olado.github.io](http://olado.github.io))

#### 特别感谢
- PDMan ([http://www.pdman.cn/](http://www.pdman.cn/))，PDMan以MIT协议开放了多年的心血，鸣谢

## 联系我们
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;***微信群***   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;***社区小程序***

&nbsp;![在这里插入图片描述](https://img-blog.csdnimg.cn/cd061ef5aabe499da197fcdbf5115902.png)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![在这里插入图片描述](https://img-blog.csdnimg.cn/8ffb80270f8347ce828dab4e1ef692ae.png)

**微信群提供给大家交流ERD，作者没那么多时间一一回复大家。如果有任何解决不了的问题，请走[VIP通道](https://portal.zerocode.net.cn/docs/benefit-profit#vip-%E9%80%9A%E9%81%93%E5%BE%AE%E4%BF%A1%E6%89%AB%E6%8F%8F%E4%B8%8B%E6%96%B9%E4%BA%8C%E7%BB%B4%E7%A0%81)**

## 加入我们
#### 开发组人员配置

我们预想的配置是：
1、后端 java 开发 2-3 名；
2、前端 raect 开发 2-3 名；
3、设计师 1 名；
4、运营相关人员 1-2 名；
5、测试人员 1 名；


> 目前已经有react+java全栈(作者自己)，设计师一名，最近添加一名vue + java 全栈

#### 开发组人员技术期望

- 期望一：后端 java 开发人员有 2 年以上的后端开发经验；
- 期望二：前端 react 开发有 1 年以上的 react 使用经验；
- 期望三：设计师有 1 年以上的平面或应用产品设计经验；
- 期望四：运营相关人员有 1 年以上的运营经验；
- 期望五：测试人员有 1 年以上的自动化测试经验；

> 当然，也欢迎各个技术栈入门工程师，我们提供全套入门指导

#### 开发组人员时间期望
- 期望一：每周工作日晚上至少有 2 天能够用于开发；
- 期望二：周末至少有 1 天能够用于开发；
- 期望三：出现紧急问题时，最好能够在当天调整时间处理；
#### 收益相关问题
这是一个长线运营的项目，所以前期是不会出经济的。我们合作的方式也是技术合作，没有薪资。所以感兴趣的朋友，也要**慎重考虑**。

团队的主要收入是,企业商业授权、定制开发、部署服务等，如果运作得好，后期是会融资的。具体的人员分工和分成具体再定，目前没有明确的比例。

如果你对这个项目感兴趣，同时也比较符合上面的期望，可以联系作者，添加上方添加微信群后，直接@群主。

## 新版本介绍及功能规划视频记录：

 - [20220311：ERD ONLINE新版本介绍及功能规划](https://www.bilibili.com/video/BV113411W7bx/)
 - [20220317：ERD ONLINE功能进度演示、头脑风暴](https://www.bilibili.com/video/BV1tu411z7bf/)
 
 
 ## 团队文档相关
 - UI设计文档: [https://js.design/f/3iRGn_?p=4rCfiHWVrO](https://js.design/f/3iRGn_?p=4rCfiHWVrO)

## 源码 OR 产品，你要什么？

[ERD未来该如何更好的服务大众](https://www.zerocode.net.cn/thread/38)



 ## 开发进度说明
 
 
| 模块功能 | 说明 | 开发完成时间|
| -------- | -------- | -------- |
| 核心功能     | 数据建模，导出，等等     | 2022-5-1     |
| 团队协作     | 多团队协作     | 2022-11-20    |
| 关系图     | 数据血缘关系   | 2022-6-20    |
| sql在线运行     | 数据库在线sql运行审计| 排期    |


## 版本规划

- **v4.0.0**
    - 全新改版，核心功能完成，已发版
- **v4.0.3**
    - 增加关系图
    - 删除、修改项目
- **v4.0.5**
    - 在线sql、查看执行计划
    - k8s快速部署
    - docker 快速部署
    - 环境变量提取
- **v5.0.0**
    - 发布企业级贴心功能，增加金融级安全机制



 
