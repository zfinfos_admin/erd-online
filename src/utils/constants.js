let urlPrefix = '';
switch (process.env.NODE_ENV) {
    case 'development':
        urlPrefix = 'http://127.0.0.1/erd'
        break
    default:
        urlPrefix = 'https://erd.java2e.com/erd'
}

export default {
    urlPrefix
}