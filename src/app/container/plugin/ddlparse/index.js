import React from 'react'
import { Input } from 'antd'
import UploadFile from './uploadFile'
import { Select } from '../../../../components';

// 解析DDL文件
export default class ReadDDLFile extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      dbType: 'mysql'
    }
  }
  _uploadSuccess = () => {
    this.props.success();
  }
  render() {
    const { dbType, moduleName } = this.state;
    
    return (
      <div className='erd-readddl'>
        <div style={{ marginTop: '20px', width: '100%' }}>
          <span style={{display: 'inline-block', width: 200, textAlign: 'right', marginRight: 20 }}>DDL类型</span>
          <Select style={{display: 'inline-block', width: 200}}
            onChange={(e)=>{
              this.setState({dbType: e.target.value});
              this.child.setState({data: { dbType: e.target.value, moduleName}});
            }}
          >
              <option value='mysql'>
                mysql
              </option>
              <option value='oracle'>
                oracle
              </option>
              <option value='sqlserver'>
                sqlserver
              </option>
              <option value='postgresql'>
                postgresql
              </option>
              <option value='hive'>
                hive
              </option>
          </Select>
        </div>
        <div style={{ marginTop: '20px', width: '100%' }}>
          <span style={{display: 'inline-block', width: 200, textAlign: 'right', marginRight: 20 }}>模块名称</span>
          <Input style={{display: 'inline-block', width: 200}} value={moduleName} placeholder="模块名称" onChange={(e)=>{
            this.setState({moduleName: e.target.value});
            this.child.setState({data: { dbType, moduleName: e.target.value}});
          }}/>
        </div>
        <div style={{ marginTop: '20px', marginBottom: '20px', width: '100%' }}>
          <span style={{display: 'inline-block', width: 200, textAlign: 'right', marginRight: 20 }}>选择要导入的文件</span>
          <UploadFile disabled={!moduleName} moduleName={moduleName} style={{display: 'inline-block'}} data={{dbType, moduleName}} accept="*" btText="点击文件上传" hint="" onClick={this._onClick} onSuccess={this._uploadSuccess} ref={e=>this.child = e} />
        </div>
      </div>
    )
  }
}
